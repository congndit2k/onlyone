using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundAsset", menuName = "ScriptableObjects/SoundAsset", order = 2)]
[System.Serializable]
public class SoundAsset : ScriptableObject
{

    public SoundManager.Sound soundType;
    public AudioClip audioClip;

}