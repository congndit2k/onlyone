using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SaveManager))]
public class GameDataManager : MonoBehaviour
{


    public static event Action<GameData> EnergyUpdated;
    public static event Action<GameData> FundsUpdated;


    [SerializeField] GameData m_GameData;
    public GameData GameData { set => m_GameData = value; get => m_GameData; }

    SaveManager m_SaveManager;
    bool m_IsGameDataInitialized;
    private void Awake()
    {
        m_SaveManager= GetComponent<SaveManager>(); 
    }

    private void Start()
    {
        m_SaveManager?.LoadGame();
    }


}
