using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int gold = 500;
    public int gem = 0;
    public int energy = 25;
    public int levelUppotions = 80;

    public string username;

    public float musicVolume;
    public float sfxVolume;


    public string toJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadJson(string jsonFilepath)
    {
        JsonUtility.FromJsonOverwrite(jsonFilepath, this);
    }

}
