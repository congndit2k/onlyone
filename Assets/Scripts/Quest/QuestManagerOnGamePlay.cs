using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManagerOnGamePlay : MonoBehaviour
{
    public List<Quest> CurrentQuests;

    private void Awake()
    {
        foreach (var quest in CurrentQuests)
        {

            quest.Initialize();
            quest.completedEvent.AddListener(OnQuestCompleted);

        }
    }

    private void OnQuestCompleted(Quest quest)
    {
    }
}
