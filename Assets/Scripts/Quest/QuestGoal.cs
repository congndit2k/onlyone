using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static Quest;

[System.Serializable]
public abstract class QuestGoal : ScriptableObject
{

    [Header("Reward")] public Stat reward = new Stat { currency = 10, xp = 10 };

    public bool hadReceived { get; private set; }

    protected string description;
    public int requiredAmount = 1;
    public int currentAmount { get; protected set; }
    public bool completed { get; protected set; }
    [HideInInspector] public UnityEvent goalCompleted;

    public virtual void Initialize()
    {
        hadReceived = false;
        completed = false;
        goalCompleted = new UnityEvent();

    }

    protected void Evaluate()
    {
        if (currentAmount >= requiredAmount)
        {
            Complete();
        }
    }

    private void Complete()
    {
        completed = true;
        goalCompleted.Invoke();
        goalCompleted.RemoveAllListeners();
    }

    public void Skip()
    {

        Complete();
    }

    public void ReceivedReward()
    {
        if (!hadReceived && completed)
        {

            hadReceived = true;
        }
    }

    public virtual string GetDescription()
    {
        return description;
    }

}
