using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KillQuest : QuestGoal
{
    public string nameEnemy;

    public override void Initialize()
    {
        base.Initialize();
        EventManager.Instance().AddListener<KillingEnemyGameEvent>(OnKilling);
    }

    public override string GetDescription()
    {
        return $"{nameEnemy}";
    }

    private void OnKilling(KillingEnemyGameEvent eventInfo)
    {

        if (eventInfo.nameEnemy == nameEnemy)
        {
            currentAmount++;
            Evaluate();
        }
    }
}
