using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class QuestWindow : MonoBehaviour
{
    [SerializeField] private TMP_Text titleTxt;
    [SerializeField] private TMP_Text descriptionTxt;
    [SerializeField] private GameObject goalPrefab;
    [SerializeField] private Transform goalContent;
    [SerializeField] private TMP_Text xpTxt;
    [SerializeField] private TMP_Text cointTxt;

    public void Initialize(Quest quest)
    {
        titleTxt.text = quest.infomation.name;
        descriptionTxt.text = quest.infomation.description;

        cointTxt.text = quest.reward.currency +"";
        xpTxt.text = quest.reward.xp +"";

        foreach (var goal in quest.goals)
        {
            GameObject goalObj = Instantiate(goalPrefab, goalContent);
            goalObj.transform.Find("Text").GetComponent<TMP_Text>().text = goal.GetDescription();
            GameObject countObj = goalObj.transform.Find("Count").gameObject;



            if (goal.completed)
            {
                Debug.Log(goal.GetDescription());
                countObj.SetActive(false);
                goalObj.transform.Find("Done").gameObject.SetActive(true);
            }
            else
            {
                countObj.GetComponent<TMP_Text>().text = goal.currentAmount + "/" + goal.requiredAmount;
            }

        }

    }

    public void CloseWindow()
    {
        gameObject.SetActive(false);
        for (int i = 0; i < goalContent.childCount; i++)
        {
            Destroy(goalContent.GetChild(i).gameObject);
        }
    }
}
