using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    [SerializeField] private GameObject questPrefab;
    [SerializeField] private Transform questContent;
    [SerializeField] private GameObject questHolder;

    public List<Quest> CurrentQuests;

    private void Awake()
    {
     foreach(var quest in CurrentQuests)
        {
            //quest.Initialize();
            //quest.completedEvent.AddListener(OnQuestCompleted);
            GameObject questObj = Instantiate(questPrefab, questContent);
            questHolder.GetComponent<Button>().onClick.AddListener(delegate
            {
                questObj.GetComponent<QuestWindow>().Initialize(quest);
                questObj.SetActive(true);
            });
        }
    }

    private void OnQuestCompleted(Quest quest)
    {
        questContent.GetChild(CurrentQuests.IndexOf(quest)).Find("Checkmark").gameObject.SetActive(true);
    }
}
