using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "ScriptableObjects/Quest", order = 1)]
[System.Serializable]
public class Quest : ScriptableObject
{
    [System.Serializable]
    public struct Info
    {
        public string name;
        public Sprite icon;
        public string description;


    }
    [Header("info")] public Info infomation; 
    
    [System.Serializable]
    public struct Stat
    {
        public int currency;
        public int xp;
    }
    [Header("Reward")] public Stat reward = new Stat {currency = 10, xp = 10 };

    public bool completed { get; private set; }
    public QuestCompletedEvent completedEvent;
    public List<QuestGoal> goals;

    public void Initialize()
    {
        completed= false;
        completedEvent= new QuestCompletedEvent();
        foreach(var goal in goals)
        {
            goal.Initialize();
            goal.goalCompleted.AddListener(delegate { CheckGoals(); });
        }
        
    }   
    private void CheckGoals()
    {
        completed = goals.All(g => g.completed);
        if(completed)
        {
            completedEvent.Invoke(this);
        }
    }


    public void GiveReward()
    {

    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(Quest))]
public class QuestEditor : Editor
{
    SerializedProperty m_QuestInfoProperty;
    SerializedProperty m_QuestStatProperty;

    List<string> m_QuestGoalType;
    SerializedProperty m_QuestGoalListProperty;

    [MenuItem("Assets/Quest", priority =0)]
    public static void CreateQuest()
    {
        var newQuest = CreateInstance<Quest>();
        ProjectWindowUtil.CreateAsset(newQuest, "quest.asset");
    }

    private void OnEnable()
    {
        m_QuestInfoProperty = serializedObject.FindProperty(nameof(Quest.infomation));
        m_QuestStatProperty = serializedObject.FindProperty(nameof(Quest.reward));

        m_QuestGoalListProperty = serializedObject.FindProperty(nameof(Quest.goals));

        var lookup = typeof(QuestGoal);
        m_QuestGoalType = System.AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(a => a.GetTypes())
            .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(lookup))
            .Select(x => x.Name)
            .ToList();
    }

    public override void OnInspectorGUI()
    {
        var child = m_QuestInfoProperty.Copy();
        var depth = child.depth;
        child.NextVisible(true);

        EditorGUILayout.LabelField("Quest info", EditorStyles.boldLabel);
        while (child.depth > depth)
        {
            EditorGUILayout.PropertyField(child, true);
            child.NextVisible(true);
        }

        child = m_QuestStatProperty.Copy();
        depth = child.depth;
        child.NextVisible(true);

        EditorGUILayout.LabelField("Quest reward", EditorStyles.boldLabel);
        while (child.depth > depth)
        {
            EditorGUILayout.PropertyField(child, true);
            child.NextVisible(true);
        }

        int choise = EditorGUILayout.Popup("Add new Quest Goal", -1, m_QuestGoalType.ToArray());

        if(choise != -1)
        {
            var newInstance = ScriptableObject.CreateInstance(m_QuestGoalType[choise]);

            AssetDatabase.AddObjectToAsset(newInstance, target);

            m_QuestGoalListProperty.InsertArrayElementAtIndex(m_QuestGoalListProperty.arraySize);
            m_QuestGoalListProperty.GetArrayElementAtIndex(m_QuestGoalListProperty.arraySize - 1)
                .objectReferenceValue =newInstance;
        }

        Editor ed = null;
        int toDelete = -1;
        for (int i = 0; i < m_QuestGoalListProperty.arraySize; ++i)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();
            var item = m_QuestGoalListProperty.GetArrayElementAtIndex(i);
            SerializedObject obj = new SerializedObject(item.objectReferenceValue);

            Editor.CreateCachedEditor(item.objectReferenceValue, null, ref ed);

            ed.OnInspectorGUI();
            EditorGUILayout.EndVertical();

            if (GUILayout.Button("-", GUILayout.Width(32)))
            {
                toDelete = i;
            }
            EditorGUILayout.EndHorizontal();

        }

        if (toDelete != -1)
        {
            var item = m_QuestGoalListProperty.GetArrayElementAtIndex(toDelete).objectReferenceValue;
            DestroyImmediate(item, true);


            //need to do it twice, first time just nullify the empty, second actually remove it
            m_QuestGoalListProperty.DeleteArrayElementAtIndex(toDelete);
            //m_QuestGoalListProperty.DeleteArrayElementAtIndex(toDelete);
        }

        serializedObject.ApplyModifiedProperties();

    }
}

#endif