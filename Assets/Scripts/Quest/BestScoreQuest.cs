﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestScoreQuest : QuestGoal
{
    public int bestScore;

    public override void Initialize()
    {
        base.Initialize();
        EventManager.Instance().AddListenerOnce<BestScoreGameEvent>(CompareScore);
    }

    private void CompareScore(BestScoreGameEvent e)
    {
        if (e.score > bestScore)
            bestScore = e.score;
    }
}
