using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static Enemy;

public class Enemy : MonoBehaviour
{
    //public enum WeakNess
    //{
    //    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
    //}
    public enum Weakness
    {
        a, c, n, e, w,o, u,
    }
    [Serializable]
    public struct EnemyData
    {
        public string name;
        public int level;
        public float speed;
        public int numOfWeakness;
        public void Set(string name,int level, float speed, int numOfWeakness)
        {
            this.name = name;
            this.level = level;
            this.speed = speed;
            this.numOfWeakness = numOfWeakness;
        }
    }

    private Rigidbody _rb;
    private Animator _animator;
    public TMP_Text _text;

    public EnemyData _enemyData;
    public List<Weakness> _weakness;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.velocity = new Vector3(0, 0, -_enemyData.speed);
        _animator = GetComponent<Animator>();
        _animator.SetFloat("Forward", _enemyData.speed / 10);
    }

    private void Update()
    {
        if (_weakness != null && _weakness.Count > 0)
            _text.text = Enum.GetName(typeof(Weakness), _weakness[0]);
        if(_rb.velocity.z < -_enemyData.speed )
            _rb.velocity = new Vector3(0, 0, -_enemyData.speed);
        if (_rb.velocity.z > -_enemyData.speed/2)
            _rb.velocity = new Vector3(0, 0, -_enemyData.speed);

    }

    public void AddWeakness()
    {
        if (_weakness == null)
            _weakness = new List<Weakness>();
        Array values = Enum.GetValues(typeof(Weakness));
        System.Random random = new System.Random();
        for (int i = 0; i < _enemyData.numOfWeakness; i++)
        {
            _weakness.Add((Weakness)values.GetValue(random.Next(values.Length)));
        }
    }
    public void AddWeakness(Weakness[] weakness)
    {
        if (_weakness == null)
            _weakness = new List<Weakness>();
        _weakness.AddRange(weakness);
    }

    public void CheckWeakness(Weakness num)
    {
        if (num == _weakness[0])
        {
            _weakness.RemoveAt(0);
        }

    }

    public bool IsDead()
    {
        return _weakness.Count == 0;
    }

    public void DestroyEnemy()
    {
        GameEvent e = new KillingEnemyGameEvent(_enemyData.name);
        EventManager.Instance().TriggerEvent(e);
        Destroy(gameObject);
    }

}
