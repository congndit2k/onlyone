using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : ScriptableObject
{
    [System.Serializable]
    public struct Info
    {
        public string name;
        public int level;
        public string description;
    }
    [Header("Infomation")] public Info infomation;

    Enemy boss;
    List<Enemy> minion;


}
