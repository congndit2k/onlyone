using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CRecognitionManager : MonoBehaviour
{
    private static CRecognitionManager _instance;

    public static CRecognitionManager Instance()
    {
        if(_instance == null)
            _instance = FindObjectOfType<CRecognitionManager>();
        return _instance; 
    }

    private GameManager gameManager => GameManager.Instance();

    [SerializeField] private DrawLines _drawLines;
    [SerializeField] private TextMeshProUGUI _recognitionResult;
    [SerializeField] private GestureTemplateData templateData;

    private GestureTemplates _templates => GestureTemplates.Get();
    private static readonly DollarOneRecognizer _dollarOneRecognizer = new DollarOneRecognizer();
    private IRecognizer _currentRecognizer = _dollarOneRecognizer;

    private List<GestureTemplate> RawTemplates = new List<GestureTemplate>();


    private void Start()
    {
        _drawLines.OnDrawFinished += OnDrawFinished;
        _currentRecognizer = _dollarOneRecognizer;
        RawTemplates = templateData.datas;
    }


    public void OnDrawFinished(PointR[] points)
    {
        _recognitionResult.text = " " + _templates.RawTemplates.Count;

        (string, float) result = _currentRecognizer.DoRecognition(points, 64,
            _templates.RawTemplates);
        //(string, float) result = _currentRecognizer.DoRecognition(points, 64,
        //    RawTemplates);
        string resultText = "";
        resultText = $"Recognized: {result.Item1}, Score: {result.Item2}";
        gameManager.CheckEnemy(result.Item1);
        //_recognitionResult.text = resultText;

    }

    public void StopRecognition()
    {
        _drawLines.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }    

    public void StartRecognition()
    {
        _drawLines.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }

    private void OnApplicationQuit()
    {
        _templates.Save();
    }
}
