using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] FloatVariable bestPoint;
    [SerializeField] TMP_Text _pointTxt;
    [SerializeField] TMP_Text _bestPointTxt;


    public void Setup(int score)
    {
        if(score > bestPoint.Value)
        {
            bestPoint.SetValue(score);
            transform.Find("NewBest").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("NewBest").gameObject.SetActive(false);

        }
        gameObject.SetActive(true);
        _pointTxt.text = score.ToString() + "POINT";
        _bestPointTxt.text ="BEST " + bestPoint.Value + " POINT";
    }

}
