using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using UnityEngine;

[RequireComponent(typeof(GameDataManager))]
public class SaveManager : MonoBehaviour
{

    GameDataManager m_gameDataManager;
    [SerializeField] string m_SaveFilename = "savegame.dat";

    private void Awake()
    {
        m_gameDataManager = GetComponent<GameDataManager>();
    }

    public GameData NewGame()
    {
        return new GameData();
    }

    public void LoadGame()
    {
        if(m_gameDataManager.GameData == null)
        {
            m_gameDataManager.GameData = NewGame();
        }
        else if(FileManager.LoadFromFile(m_SaveFilename, out var jsonString))
        {

        }

    }
}
