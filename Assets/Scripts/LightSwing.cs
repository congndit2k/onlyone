using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwing : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform playerTranf;
    [SerializeField] private float time;
    [SerializeField] private float radius;

    private void Update()
    {
        transform.rotation = new Quaternion(playerTranf.rotation.x, playerTranf.rotation.y, transform.rotation.z,0);
    }
}
