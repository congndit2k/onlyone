using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameEvent
{

}

public class KillingEnemyGameEvent :GameEvent
{
    public string nameEnemy;

    public KillingEnemyGameEvent(string nameEnemy)
    {
        this.nameEnemy = nameEnemy;
    }
}

public class BestScoreGameEvent : GameEvent
{
    public int score;
}

public class WarningGameEvent : GameEvent
{
    public enum WarningType
        {
        BOSS_COMING,
        ENEMY_NEAR,
        TIME_OVER
    }

    public WarningType type;
    public float timeDistance;
}


public class WhoIsBossNowGameEvent : GameEvent
{
    public string bossName;
}
