using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    [System.Serializable]
    public enum Sound
    {
        BossComing,
        OnDanger,
        GameOver,
        EnemyDie,
        TimeOver,
    }
    [SerializeField] private List<SoundAsset> soundAssets;
    private Dictionary<Sound, float> soundTimerDictionary;
    public void PlaySound(Sound sound)
    {
        GameObject soundGameObj = new GameObject("sound");
        AudioSource audioSource = soundGameObj.AddComponent<AudioSource>();
        audioSource.PlayOneShot(GetAudioClip(sound));
    }

    private AudioClip GetAudioClip(Sound sound)
    {
        foreach (SoundAsset soundAsset in soundAssets)
        {
            if (soundAsset.soundType == sound)
                return soundAsset.audioClip;
        }
        Debug.LogError("Sound" + sound + " not found!");
        return null;
    }

    public void Inittialize()
    {
        soundTimerDictionary = new Dictionary<Sound, float>();
        soundTimerDictionary[Sound.OnDanger] = 0f;
        EventManager.Instance().AddListener<WarningGameEvent>(WaringSound);
        EventManager.Instance().AddListener<KillingEnemyGameEvent>(EnemyDieSound);

    }

    private void Start()
    {
        Inittialize();
    }

    private void EnemyDieSound(KillingEnemyGameEvent e)
    {
        PlaySound(Sound.EnemyDie);
    }

    private void WaringSound(WarningGameEvent e)
    {
        switch (e.type)
        {
            case WarningGameEvent.WarningType.BOSS_COMING:
                PlaySound(Sound.BossComing);
                break;
            case WarningGameEvent.WarningType.ENEMY_NEAR:
                if (soundTimerDictionary.ContainsKey(Sound.OnDanger))
                {
                    float lastTime = soundTimerDictionary[Sound.OnDanger];
                    float maxTime = e.timeDistance;
                    if (lastTime + maxTime < Time.time)
                    {
                        soundTimerDictionary[Sound.OnDanger] = Time.time;
                        PlaySound(Sound.OnDanger);
                    }
                }
                break;
            case WarningGameEvent.WarningType.TIME_OVER:
                PlaySound(Sound.TimeOver);
                break;


        }
    }

}

