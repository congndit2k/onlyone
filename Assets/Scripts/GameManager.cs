using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;
using static GameManager;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    [SerializeField] GameOverScreen _overScreen;
    [SerializeField] TMP_Text _scoresTxt;
    [SerializeField] GameObject _pausePanel;

    public EnemyManager _enemyManager => EnemyManager.Instance();
    private Player _player => Player.Instance();

    private static bool isPause = false;
    private static bool onDanger = false;
    private bool gameOver = false;
    [SerializeField] private float _dangerZone = 5f;
    public List<Enemy> _enemies;
    public int _scores = 0;


    public static GameManager Instance()
    {
        if (_instance == null)
            _instance = FindObjectOfType<GameManager>();
        return _instance;

    }
    private void Awake()
    {
        _scores = 0;
        Time.timeScale = 1.0f;
    }
    private void Start()
    {
        _enemies = _enemyManager._enemies;

    }

    private void Update()
    {

        _scoresTxt.text = _scores + "";
        int count = 0;
        for (int i = 0; i < _enemies.Count; i++)
        {

            if (_enemies[i].gameObject.transform.position.z <= _player.gameObject.transform.position.z)
            {
                //_enemies[i].gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                GameOver();
            }
            else if (_enemies[i].gameObject.transform.position.z <= _player.gameObject.transform.position.z + _dangerZone 
                && !gameOver)
            {
                    EventManager.Instance().TriggerEvent(new WarningGameEvent { type = WarningGameEvent.WarningType.ENEMY_NEAR, timeDistance = 0.9f });
                    onDanger = true;

            }


        }

    }

    public void CheckEnemy(string id)
    {
        onDanger = false;

        if (Enum.IsDefined(typeof(Enemy.Weakness), id))
            for (int i = 0; i < _enemies.Count; i++)
            {
                _enemies[i].CheckWeakness((Enemy.Weakness)Enum.Parse(typeof(Enemy.Weakness), id));
            }
    }


    public void GameInit()
    {
        _scores = 0;
    }



    public void GameOver()
    {
        gameOver = true;
        CRecognitionManager.Instance().StopRecognition();
        PauseGame();
        _overScreen.Setup(_scores);
    }

    public void ResetGame()
    {
        ResumeGame();
        _enemyManager.ResetEnemyManager();
    }

    public void Restart()
    {
        ResumeGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void SetPausePanel()
    {
        if (isPause)
        {
            CRecognitionManager.Instance().StartRecognition();
            _pausePanel.SetActive(false);
            ResumeGame();
        }
        else
        {
            CRecognitionManager.Instance().StopRecognition();
            _pausePanel.SetActive(true);
            PauseGame();

        }
    }

    private void PauseGame()
    {

        Time.timeScale = 0;
        isPause = true;

    }

    private void ResumeGame()
    {

        Time.timeScale = 1;
        isPause = false;

    }

}
