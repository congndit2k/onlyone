using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBossOne : MonoBehaviour
{
    [SerializeField] private int numberEnemy = 3;
    private void OnDestroy()
    {
        for (int i = 0; i < numberEnemy; i++)
        {
            EnemyManager.Instance().SpawnerEnemy(1, transform);
        }
    }
}
