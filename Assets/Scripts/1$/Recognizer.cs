using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Recognizer
{
    public Vector2 GetCentroid(PointR[] points)
    {
        float centerX = points.Sum(point => point.point.x) / points.Length;
        float centerY = points.Sum(point => point.point.y) / points.Length;
        return new Vector2(centerX, centerY);
    }

    public PointR[] ResamplePoints(PointR[] points, int n)
    {
        float incrementValue = PathLength(points) / (n - 1);
        float proceedDistance = 0;
        List<PointR> newPoints = new List<PointR> { points[0] };
        for (int i = 1; i < points.Length; i++)
        {
            PointR previousPoint = points[i - 1];
            PointR currentPoint = points[i];
            if(previousPoint.T == currentPoint.T)
            {
                float distance = Vector2.Distance(previousPoint.point, currentPoint.point);
                if (proceedDistance + distance >= incrementValue)
                {
                    while (proceedDistance + distance >= incrementValue)
                    {
                        float t = Math.Min(Math.Max((incrementValue - proceedDistance) / distance, 0.0f), 1.0f);
                        if (float.IsNaN(t)) t = 0.5f;

                        float approximatedX =
                            previousPoint.point.x +
                            t * (currentPoint.point.x - previousPoint.point.x);
                        float approximatedY =
                            previousPoint.point.y +
                            t * (currentPoint.point.y - previousPoint.point.y);
                        PointR approximatedPoint = new PointR(approximatedX, approximatedY, previousPoint.T);
                        newPoints.Add(approximatedPoint);

                        distance = proceedDistance + distance - incrementValue;
                        proceedDistance = 0;
                        previousPoint = newPoints[newPoints.Count - 1];
                    }

                    proceedDistance = distance;
                }
                else
                {
                    proceedDistance += distance;
                }
            }
        }

        if (proceedDistance > 0)
        {
            newPoints.Add(points[points.Length - 1]);
        }

        return newPoints.ToArray();

    }

    public float PathLength(PointR[] points)
    {
        float length = 0;
        for (int i = 1; i < points.Length; i++)
        {
            PointR previous = points[i - 1];
            PointR current = points[i];
            float distance = Vector2.Distance(previous.point, current.point);
            if (!float.IsNaN(distance) && previous.T == current.T)
            {
                length += distance;
            }
        }

        return length;
    }
}
