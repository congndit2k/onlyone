using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DollarOneRecognizer : Recognizer, IRecognizer
{
    private int _size = 250;

    public enum Step
    {
        RAW,
        RESAMPLED,
        ROTATED,
        SCALED,
        TRANSLATED
    }

    public PointR[] Normalize(PointR[] points, int n, Step step = Step.TRANSLATED)
    {
        PointR[] copyPoints = new PointR[points.Length];
        points.CopyTo(copyPoints, 0);
        PointR[] resampledPoints = ResamplePoints(copyPoints, n);
        PointR[] rotatedPoints = RotateToZero(resampledPoints);
        PointR[] scaledPoints = ScaleToSquare(rotatedPoints, _size);
        PointR[] translatedToOrigin = TranslateToOrigin(scaledPoints);

        //Debug purpose only
        switch (step)
        {
            case Step.RAW:
                return points;
            case Step.RESAMPLED:
                return resampledPoints;
            case Step.ROTATED:
                return rotatedPoints;
            case Step.SCALED:
                return scaledPoints;
        }

        return translatedToOrigin;
    }

    public (string, float) DoRecognition(PointR[] points, int n,
        List<GestureTemplate> gestureTemplates)
    {
        PointR[] preparedPoints = Normalize(points, n);
        float angle = 0.5f * (-1 + Mathf.Sqrt(5));
        return Recognize(preparedPoints, gestureTemplates, 250, angle);
    }


    private PointR[] RotateToZero(PointR[] points)
    {
        float angle = IndicativeAngle(points);
        List<PointR> newPoints = RotateBy(points, -angle);
        return newPoints.ToArray();
    }

    private List<PointR> RotateBy(PointR[] points, float angle)
    {
        List<PointR> newPoints = new List<PointR>(points.Length);
        Vector2 centroid = GetCentroid(points);
        foreach (PointR point in points)
        {
            float rotatedX = (point.point.x - centroid.x) * Mathf.Cos(angle) -
                             (point.point.y - centroid.y) * Mathf.Sin(angle) +
                             centroid.x;
            float rotatedY = (point.point.x - centroid.x) * Mathf.Sin(angle) +
                             (point.point.y - centroid.y) * Mathf.Cos(angle) +
                             centroid.y;
            newPoints.Add(new PointR(rotatedX, rotatedY, 0));
        }

        return newPoints;
    }

    private float IndicativeAngle(PointR[] points)
    {
        Vector2 centroid = GetCentroid(points);
        return Mathf.Atan2(points[0].point.y - centroid.y, points[0].point.x - centroid.x);
    }

    private Vector2 GetCentroid(Vector2[] points)
    {
        float centerX = points.Sum(point => point.x) / points.Length;
        float centerY = points.Sum(point => point.y) / points.Length;
        return new Vector2(centerX, centerY);
    }

    private PointR[] ScaleToSquare(PointR[] points, float size)
    {
        List<PointR> newPoints = new List<PointR>(points.Length);
        Rect box = GetBoundingBox(points);
        foreach (PointR point in points)
        {
            float scaledX = point.point.x * size / box.width;
            float scaledY = point.point.y * size / box.height;
            newPoints.Add(new PointR(scaledX, scaledY, 0));
        }

        return newPoints.ToArray();
    }

    private Rect GetBoundingBox(PointR[] points)
    {
        float minX = points.Select(point => point.point.x).Min();
        float maxX = points.Select(point => point.point.x).Max();
        float minY = points.Select(point => point.point.y).Min();
        float maxY = points.Select(point => point.point.y).Max();
        return new Rect(minX, minY, maxX - minX, maxY - minY);
    }

    private PointR[] TranslateToOrigin(PointR[] points)
    {
        List<PointR> newPoints = new List<PointR>(points.Length);
        Vector2 centroid = GetCentroid(points);
        foreach (PointR point in points)
        {
            float translatedX = point.point.x - centroid.x;
            float translatedY = point.point.y - centroid.y;
            newPoints.Add(new PointR(translatedX, translatedY, 0));
        }

        return newPoints.ToArray();
    }

    private (string, float) Recognize(
        PointR[] points,
        List<GestureTemplate> gestureTemplates,
        float size,
        float angle)
    {
        float theta = 45;
        float deltaTheta = 2;
        float bestDistance = float.MaxValue;
        GestureTemplate bestTemplate = new GestureTemplate();

        //Should be stored in proceesed, but for testing purpose we use RawPoints
        IEnumerable<GestureTemplate> proceedGestures = gestureTemplates.Select(template =>
            new GestureTemplate() { Points = Normalize(template.Points, 64), Name = template.Name });

        foreach (GestureTemplate gestureTemplate in proceedGestures.Where(template =>
            template.Points.Length == points.Length))
        {
            float distance = DistanceAtBestAngle(points, gestureTemplate, -theta, theta, deltaTheta, angle);
            if (distance < bestDistance)
            {
                bestDistance = distance;
                bestTemplate = gestureTemplate;
            }
        }

        double score = 1 - (bestDistance / (0.5f * Math.Sqrt(2 * size * size)));
        return ((string, float))(bestTemplate.Name, score);
    }

    private float DistanceAtBestAngle(PointR[] points, GestureTemplate template, float thetaA,
        float thetaB,
        float deltaTheta, float angle)
    {
        float firstX = angle * thetaA + (1 - angle) * thetaB;
        float firstDistance = DistanceAtAngle(points, template, firstX);
        float secondX = (1 - angle) * thetaA + angle * thetaB;
        float secondDistance = DistanceAtAngle(points, template, secondX);

        while (thetaB - thetaA > deltaTheta)
        {
            if (firstDistance < secondDistance)
            {
                thetaB = secondX;
                secondX = firstX;
                secondDistance = firstDistance;
                firstX = angle * thetaA + (1 - angle) * thetaB;
                firstDistance = DistanceAtAngle(points, template, firstX);
            }
            else
            {
                thetaA = firstX;
                firstX = secondX;
                firstDistance = secondDistance;
                secondX = (1 - angle) * thetaA + angle * thetaB;
                secondDistance = DistanceAtAngle(points, template, secondX);
            }
        }

        return Mathf.Min(firstDistance, secondDistance);
    }

    private float DistanceAtAngle(PointR[] points, GestureTemplate template, float angle)
    {
        List<PointR> newPoints = RotateBy(points, angle);
        return PathDistance(newPoints, template.Points);
    }

    private float PathDistance(List<PointR> points, PointR[] templatePoints)
    {
        float distance = 0;

        for (int i = 0; i < points.Count; i++)
        {
            distance += Vector2.Distance(points[i].point, templatePoints[i].point);
        }

        return distance / points.Count;
    }

}
