using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRecognizer
{
    public PointR[] Normalize(PointR[] points, int n, 
        DollarOneRecognizer.Step step = DollarOneRecognizer.Step.TRANSLATED);

    public (string, float) DoRecognition(PointR[] points, int n,
        List<GestureTemplate> gestureTemplates);
}
