using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

[Serializable]
public struct PointR
{
    public Vector2 point;
    public int T;

    public PointR(float x, float y, int t)
    {
        point = new Vector2(x, y);
        T = t;
    }



    public PointR(PointR other)
    {
        point = other.point;
        T = other.T;
    }

}
