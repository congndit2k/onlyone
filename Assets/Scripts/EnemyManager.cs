using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static Enemy;

public class EnemyManager : MonoBehaviour
{

    private static EnemyManager _instance;
    private Spawner _Spawner => Spawner.Instance();
    private GameManager _gameManager => GameManager.Instance();

    [SerializeField] EnemySpawer[] _enemyDatalist;
    [SerializeField] EnemySpawer _boss;

    public List<Enemy> _enemies;
    public float _waitingTime = 0.4f;
    private float _timeStart;
    private bool _spawningMinion = true;
    private bool _bossIsComing = false;

    [Serializable]
    struct EnemySpawer
    {
        public Enemy.EnemyData data;
        public GameObject obj;
    }

    public static EnemyManager Instance()
    {
        if (_instance == null)
            _instance = FindObjectOfType<EnemyManager>();
        return _instance;

    }

    private void Awake()
    {
        _enemies = new List<Enemy>();
    }

    private void Start()
    {
        SpawnerRandomEnemy();
    }

    private void Update()
    {
        _timeStart += Time.deltaTime;
        if(_gameManager._scores >= 10 &&!_bossIsComing)
        {
            Debug.Log(_gameManager._scores + "");
            _spawningMinion = false;
            SpawnerBoss();
            _bossIsComing = true;
            EventManager.Instance().TriggerEvent(new WarningGameEvent { type = WarningGameEvent.WarningType.BOSS_COMING});
        }
        if (_timeStart >= _waitingTime && _spawningMinion)
        {
            if (_enemies.Count < 10)
                SpawnerRandomEnemy();
            _timeStart = 0;
        }

        for (int i = 0; i < _enemies.Count; i++)
        {
            if (_enemies[i].IsDead())
            {
                _enemies[i].DestroyEnemy();
                DontDestroyOnLoad(_enemies[i].gameObject);
                _gameManager._scores += _enemies[i]._enemyData.level;
                _enemies.RemoveAt(i);
            }
        }
    }

    void SpawnerRandomEnemy()
    {
        System.Random random = new System.Random();

        int num = random.Next(0, 3);
        Enemy ene = _enemyDatalist[num].obj.GetComponent<Enemy>();
        ene._enemyData.Set(_enemyDatalist[num].data.name, _enemyDatalist[num].data.level, _enemyDatalist[num].data.speed, _enemyDatalist[num].data.numOfWeakness);
        _enemies.Add(_Spawner.SpawerEnemy(_enemyDatalist[num].obj));
    }

    void SpawnerBoss()
    {
        Enemy ene = _boss.obj.GetComponent<Enemy>();
        ene._enemyData.Set(_boss.data.name,_boss.data.level, _boss.data.speed, _boss.data.numOfWeakness);
        _enemies.Add(_Spawner.SpawerEnemy(_boss.obj));
    }

    public void SpawnerEnemy(int enemyLevel, Transform transform)
    {
        System.Random random = new System.Random();

        EnemySpawer enemyRandom;
        do
        {
            int num = random.Next(0, _enemyDatalist.Length);
            enemyRandom = _enemyDatalist[num];
        } while (enemyRandom.data.level == enemyLevel);

        Enemy enemy = enemyRandom.obj.GetComponent<Enemy>();
        enemy._enemyData.Set(enemyRandom.data.name, enemyRandom.data.level, enemyRandom.data.speed, enemyRandom.data.numOfWeakness);
        _enemies.Add(_Spawner.SpawerEnemy(enemyRandom.obj));
    }

    public void ResetEnemyManager()
    {
        _timeStart = 0;
    }
}
