using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DrawLines : MonoBehaviour
{

    public event Action<PointR[]> OnDrawFinished;

    public Camera _camera;
    public LineRenderer _lineRenderer;

    private float PosZ = 7;
    private Vector3 _mosePos;
    private Vector3 _pos;
    private Vector3 _previonsPos;


    public List<Vector3> _linePos;
    public List<PointR> _drawPos;

    public float minimumDistance = 0.05f;
    private float distance = 0;

    private void Start()
    {

    }

    private int _strokeIndex;

    private void Update()
    {

        if (Input.touchSupported)
        {
            AndroidDraw();
        }
        else
        {
            PCDraw();
        }
    }

    void PCDraw()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _strokeIndex = 0;
            _mosePos = Input.mousePosition;
            _mosePos.z = PosZ;
            _pos = _camera.ScreenToWorldPoint(_mosePos);
            _previonsPos = _pos;
            _linePos.Add(_pos);
            _drawPos.Add(new PointR(_pos.x, _pos.y, _strokeIndex));
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _strokeIndex++;
            _strokeIndex = 0;
            PointR[] points = _drawPos.ToArray();
            Reset();
            OnDrawFinished?.Invoke(points);
        }
        else if (Input.GetMouseButton(0))
        {
            _mosePos = Input.mousePosition;
            _mosePos.z = PosZ;
            _pos = _camera.ScreenToWorldPoint(_mosePos);
            distance = Vector3.Distance(_pos, _previonsPos);
            if (distance >= minimumDistance)
            {
                _previonsPos = _pos;
                _linePos.Add(_pos);
                _drawPos.Add(new PointR(_pos.x, _pos.y, _strokeIndex));

                _lineRenderer.positionCount = _linePos.Count;
                _lineRenderer.SetPositions(_linePos.ToArray());

            }
        }

    }

    void AndroidDraw()
    {
        if (Input.touchCount <= 0)
            return;
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            _strokeIndex = 0;
            _mosePos = Input.mousePosition;
            _mosePos.z = PosZ;
            _pos = _camera.ScreenToWorldPoint(_mosePos);
            _previonsPos = _pos;
            _linePos.Add(_pos);
            _drawPos.Add(new PointR(_pos.x, _pos.y, _strokeIndex));
        }

        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            _mosePos = Input.mousePosition;
            _mosePos.z = PosZ;
            _pos = _camera.ScreenToWorldPoint(_mosePos);
            distance = Vector3.Distance(_pos, _previonsPos);
            if (distance >= minimumDistance)
            {
                _previonsPos = _pos;
                _linePos.Add(_pos);
                _drawPos.Add(new PointR(_pos.x, _pos.y, _strokeIndex));
                _lineRenderer.positionCount = _linePos.Count;
                _lineRenderer.SetPositions(_linePos.ToArray());

            }
        }
        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {    
            _strokeIndex++;
            _strokeIndex = 0;
            PointR[] points = _drawPos.ToArray();
            Reset();
            //OnDrawFinished?.Invoke(points);
            CRecognitionManager.Instance().OnDrawFinished(points);
        }

        if (Input.GetTouch(0).phase == TouchPhase.Canceled)
        {
        }
    }


    private void Reset()
    {
        _drawPos.Clear();
        _linePos.Clear();
        _pos = Vector3.zero;
        _previonsPos = Vector3.zero;
        _lineRenderer.positionCount = 0;
        _lineRenderer.ResetBounds();
    }

}
