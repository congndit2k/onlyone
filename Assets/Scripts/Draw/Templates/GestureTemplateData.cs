using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(fileName = "GestureTemplate", menuName = "ScriptableObjects/GestureTemplate", order = 1)]
public class GestureTemplateData : ScriptableObject
{

    [Header("GestureTemplate")] public List<GestureTemplate> datas;

}
