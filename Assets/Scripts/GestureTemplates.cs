using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

///Data/Data/com.CongND.OnlyOne/files

[System.Serializable]
public struct GestureTemplate
{
    public string Name;
    public PointR[] Points;

    public GestureTemplate(string templateName, PointR[] preparePoints)
    {
        Name = templateName;
        Points = preparePoints;
    }
}

[Serializable]
public class GestureTemplates
{

    private static GestureTemplates instance;

    public static GestureTemplates Get()
    {
        if (instance == null)
        {
            instance = new GestureTemplates();
            instance.LoadByResource();
        }
        return instance;
    }


    public List<GestureTemplate> RawTemplates = new List<GestureTemplate>();
    public List<GestureTemplate> ProceedTemplates = new List<GestureTemplate>();
    public TextAsset textAsset;

    public List<GestureTemplate> GetTemplates()
    {
        return ProceedTemplates;
    }

    public void RemoveAtIndex(int indexToRemove)
    {
        ProceedTemplates.RemoveAt(indexToRemove);
        RawTemplates.RemoveAt(indexToRemove);
    }

    public GestureTemplate[] GetRawTemplatesByName(string name)
    {
        return RawTemplates.Where(template => template.Name == name).ToArray();
    }

    public void Save()
    {

        string path = Application.dataPath + "/SavedTemplates.json";
        string potion = JsonUtility.ToJson(this);
        File.WriteAllText(path, potion);

    }

    private void Load()
    {

        string path = Application.dataPath + "/SavedTemplates.json";

        if (File.Exists(path))
        {

            GestureTemplates data = JsonUtility.FromJson<GestureTemplates>(File.ReadAllText(path));
            RawTemplates.Clear();
            RawTemplates.AddRange(data.RawTemplates);
            ProceedTemplates.Clear();
            ProceedTemplates.AddRange(data.ProceedTemplates);
        }


    }

    private void LoadByResource()
    {
        textAsset = Resources.Load<TextAsset>("GestureTemplate/SavedTemplates") as TextAsset;

        GestureTemplates data = JsonUtility.FromJson<GestureTemplates>(textAsset.text);
        RawTemplates.Clear();
        RawTemplates.AddRange(data.RawTemplates);
        ProceedTemplates.Clear();
        ProceedTemplates.AddRange(data.ProceedTemplates);
    }
}