using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private static Spawner _instance;

    public float _gateRadius;

    public static Spawner Instance()
    {
        if (_instance == null)
        {
            _instance = FindObjectOfType<Spawner>();
        }
        return _instance;
    }

    private void Awake()
    {
        //if(_instance != null)
        //{
        //    Destroy(this);
        //}
        //DontDestroyOnLoad(this);
        _instance= this;
    }
    public Enemy SpawerEnemy(GameObject enemy)
    {
        System.Random random = new System.Random();
        Vector3 pos = new Vector3(
            transform.position.x + random.Next((int)-_gateRadius/2, (int)_gateRadius/2)*2,
            transform.position.y,
            transform.position.z);
        GameObject eneObj = Object.Instantiate(enemy, pos, new Quaternion(0,180,0,0));

        Enemy ene = eneObj.GetComponent<Enemy>();
        ene._enemyData.level = 1;

        ene.AddWeakness();
        return ene;
    }

}
